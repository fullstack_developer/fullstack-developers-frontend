import "./globals.css";

import Link from "next/link";
import Image from "next/image";

export const metadata = {
  title: "Fullstack Developers",
  description:
    "Find experienced developers, read their blogs, and connect with them",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <div className="font-sans text-gray-900 antialiased">
          <nav className="flexBetween navbar bg-neutral-800">
            <Image src="/logo.png" width={40} height={40} alt="logo" />
            <div>
              <Link href="/" className="text-white mr-4 text-lg">
                Home
              </Link>
              <Link href="/developers" className="text-white mr-4  text-lg">
                Developers
              </Link>
              <Link href="/blogs" className="text-white mr-4 text-lg">
                Blogs
              </Link>
              <Link href="/contact" className="text-white mr-4  text-lg">
                Contact Us
              </Link>
            </div>

            <div>
              <Link href="/login" className="text-white mr-4 text-sm">
                Login
              </Link>
              <Link href="/contact" className="text-white mr-4 text-sm">
                Sign Up
              </Link>
            </div>
          </nav>

          {children}

          <footer className="bg-neutral-800 p-6 text-center text-white">
            <Link href="/privacy" className="mr-4">
              Privacy Policy
            </Link>
            <Link href="/terms" className="mr-4">
              Terms of Service
            </Link>
            <p className="mt-6">&copy; 2023 Fullstack Developers</p>
          </footer>
        </div>
      </body>
    </html>
  );
}
