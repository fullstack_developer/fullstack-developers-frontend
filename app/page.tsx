import Image from "next/image";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <div className="bg-[url('/hero-background.jpg')] bg-cover bg-center bg-no-repeat h-screen">
        <header className="bg-gray-200 text-center py-20 ">
          <h1 className="text-4xl mb-6 mt-48 text-white">
            Welcome to Fullstack Developers
          </h1>
          <p className="text-xl mb-60 text-emerald-100">
            Find experienced developers, read their blogs, and connect with them
          </p>
          <button className="bg-amber-700 hover:bg-amber-800 text-white font-bold py-3 px-4 rounded">
            Become our coleague
          </button>
        </header>
      </div>

      <main className="py-10 px-6 bg-neutral-900">
        <input
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="text"
          placeholder="Search for developers..."
        />

        <section className="py-10">
          <h2 className="text-2xl mb-6 text-white">Top Developers</h2>
          {/* List of developers */}
        </section>

        <section className="py-10">
          <h2 className="text-2xl mb-6 text-white">Recent Blogs</h2>
          {/* List of recent blog posts */}
        </section>
      </main>
    </>
  );
}
